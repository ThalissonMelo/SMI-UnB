import smi_unb


def test_project_defines_author_and_version():
    assert hasattr(smi_unb, '__author__')
    assert hasattr(smi_unb, '__version__')
